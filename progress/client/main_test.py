import time
import os
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from swpp_module import *

#setup
link = 'http://swpp.devluci.com:8000/'
browser = webdriver.Chrome('/usr/local/bin/chromedriver')
browser.get(link)
time.sleep(1)

#label login elements
login_button = browser.find_element_by_id('login_button')
login_name = browser.find_element_by_id('login_username')
login_pwd = browser.find_element_by_id('login_password')
#login
time.sleep(1)
login_name.send_keys("swpp201710")
time.sleep(1)
login_pwd.send_keys("good!morning123")
time.sleep(1)
login_button.click()
time.sleep(2)

#post test
post_text = browser.find_element_by_id('post_text')
post_button = browser.find_element_by_id('post_button')
post_text.clear()
post_content = "Post test"
post_text.send_keys(post_content)
time.sleep(1)
post_button.click()
time.sleep(1)
#find post
posts_text = []
posts_text = browser.find_elements_by_class_name('text')
if posts_text[0].text != post_content:
    print("no post found")
    browser.quit()
    exit(1)
    
#reply test
reply_text = []
reply_text = browser.find_elements_by_id('reply_text')
reply_button = []
reply_button = browser.find_elements_by_id('reply_button')
reply_text[0].clear()
reply_content = "Reply test"
reply_text[0].send_keys(reply_content)
time.sleep(1)
reply_button[0].click()
time.sleep(1)
#find reply
replies_text = []
replies_text = browser.find_elements_by_class_name('cmNhdx')
if replies_text[0].text != reply_content:
    print("no reply found")
    browser.quit()
    exit(1)

#reply load more test
for i in range(1, 11):
    reply_text[0].clear()
    reply_content = "Reply test{0}".format(i)
    reply_text[0].send_keys(reply_content)
    time.sleep(1)
    reply_button[0].click()
    time.sleep(1)
#load more and find last reply
load_more_button = []
load_more_button = browser.find_elements_by_id('loadMoreReply')
load_more_button[0].click()
time.sleep(1)
replies_text = browser.find_elements_by_class_name('cmNhdx')
if replies_text[10].text != "Reply test10":
    print("cannot load more")
    browser.quit()
    exit(1)

#like test
likes = browser.find_elements_by_class_name('like')
like_button = likes[0].find_element_by_id('like_button')
like_button.click()
time.sleep(1)
likes = browser.find_elements_by_class_name('like')
dislike_button = likes[0].find_element_by_id('dislike_button')
if dislike_button == None:
    print("cannot like")
    browser.quit()
    exit(1)

"""
like_button = []
like_button = browser.find_elements_by_id('like_button')
like_button[0].click()
time.sleep(1)
like_button = browser.find_elements_by_id('like_button')
if like_button[0].get_attribute('class') != 'dWxUBt':
    print("cannot like")
    browser.quit()
    exit(1)
"""

#cancel like test
dislike_button.click()
time.sleep(1)
likes = browser.find_elements_by_class_name('like')
like_button = likes[0].find_element_by_id('like_button')
if like_button == None:
    print("cannot cancel like")
    browser.quit()
    exit(1)

    """
    like_button = browser.find_elements_by_id('like_button')
if like_button[0].get_attribute('class') != 'kTrqtM':
    print("cannot cancel like")
    browser.quit()
    exit(1)
"""
#profile test
profile_button = browser.find_element_by_id('profile_button')
profile_button.click()
time.sleep(3)
file_path = os.path.abspath('images.jpg')
file_input = browser.find_element_by_id('fileInput')
file_input.send_keys(file_path)
upload_image_button = browser.find_element_by_id('upload_image_button')
upload_image_button.click()

#go to main
main_button = browser.find_element_by_id('Main_button')
main_button.click()
time.sleep(3)
#logout test
logout_button = browser.find_element_by_id('logout_button')
logout_button.click()
time.sleep(1)
if browser.current_url != link + 'login/':
    print("cannot logout")
    browser.quit()
    exit(1)

#check complete, exit
time.sleep(2)
browser.quit()
print("Main page test complete")
