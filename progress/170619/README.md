# A. 금주 작업한 Bahavior list
* 채팅
    * Back-End(박지환)
        * /api/message 추가
        * /api/message/from, /api/message/to 추가
    * Front-End(전영웅)
        * Chat 페이지 추가
        * 페이지 내에 Following list가 표시되며, 해당 유저들과 채팅 가능
* Post list
    * Back-End(박지환)
        * 팔로우 한 대상의 글만 보이도록 수정
* 팔로우
    * Front-End(전영웅)
        * 화면 우측에 자신이 팔로잉 하고 있는 유저 목록 추가
* 팔로우 추천
    * Back-End(박지환)
        * Prefer, Sim 모델 추가(유저 유사도)
        * Prefer 계산 봇 구현
        * Follow recommendation을 Sim을 바탕으로 이뤄지도록 구현
    * Front-End(전영웅)
        * 화면 우상단에 친구 추천 목록 추가
* 크롤링
    * Back-End(박지환)
        * 네이버 블로그 크롤링 봇 구현
* 프로필 페이지
    * Front-End(전영웅)
        * 자신 뿐 아니라 모든 유저의 프로필 페이지가 생성되어 확인 가능
* Jest 테스트 코드(박희준)
* Back-End 테스트 코드(현유지)
* Front-End 테스트 코드(현유지)


# B. Back-End
## 1. API Spec
* https://bitbucket.org/Gamhat/swpp201701project/wiki/Home

## 2. Test code
* progress/server/runtest.py
* 실행법
    * $ python3 runtest.py
* 테스트 항목
    * 기존 항목
    
    1. Checking join
    1. Checking login
    1. Checking logout
    1. Checking writing by POST /post
    1. Checking get post by GET /post
    1. Checking reply by POST, GET /post/<post id>/reply 
    1. Checking DELETE by /post/<post id>
    1. Checking DELETE by /post/<post id>/reply/<reply id>
    1. Checking like by POST, GET /post/<post id>/like
    1. Checking double likes is prohibited
    1. Checking DELETE /post/<post id>/like/<like id>
    1. Checking GET user info by /user/<user id>
    1. Checking follow by POST, GET /follow
    1. Checking double follows is prohibited
    1. Checking follow by DELETE /follow/<follow id>
    1. Checking adding tags by POST /tag
    1. Checking get tags by GET /tag    
    1. Checking GET logged in user info by /user
    1. Checking GET user's post by /user/<user_id>/post

    * 마지막에 “TEST SUCCESSFUL”이 출력될 경우 성공

# C. Front-End
## 1. Test code
* progress/client/login_test.py
    * 로그인, 조인 페이지 테스트
* progress/client/main_test.py
    * 메인 페이지 테스트
* 실행법
    * $ python3 login_test.py
    * $ python3 main_test.py
    * 순서 무관. 하나만 돌려도 상관 없음
* 테스트 항목(login_test)
    1. Check join : 가입 시 username 길이, email 형식 등 제약조건 테스트
        1. check username fail
        1. check password fail
        1. check email fail
        1. check family name fail
        1. check given name fail
        1. check join success
    1. Check login
        1. check login success
        1. check login fail
    1. Check same info : 기존 유저와 동일한 정보로 가입시 에러 발생하는 지 테스트
        1. check same username error
        1. check same email error

    * 마지막에 “Login, Join page test complete”이 출력될 경우 성공

* 테스트 항목(main_test)

    1. post test - 포스트 작성 테스트
    1. reply test - 댓글 작성 테스트
    1. reply load more test - 댓글의 더보기 버튼(load more) 테스트
    1. like test - 좋아요 테스트
    1. cancel like test - 좋아요 취소 테스트
    1. logout test - 로그아웃 테스트
    1. profile test - 프로필 페이지 동작 테스트
    1. upload profile image test - 프로필 이미지 변경 테스트

    * 마지막에 “Main page test complete”이 출력될 경우 성공

# D. Redux unit test
## 1. Test result
* Pipelines
    * Pipelines 페이지를 통해 main_page, login_page, chat_page, profile_page, my_profile_page의 test result 확인 가능
* Main page
    * ![Alt text](http://i.imgur.com/WOwXzMh.png)
* Login page
    * ![Alt text](http://i.imgur.com/BcTKK9i.png)
* Chat page
    * ![Alt text](http://i.imgur.com/E9jU5cR.png)
* My profile page
    * ![Alt text](http://i.imgur.com/6tuUsQn.png)
* Profile page(타인의 프로필 페이지)
    * ![Alt text](http://i.imgur.com/jrHqn4z.png)
    
# E. Web service 링크
* http://swpp.devluci.com:8000/