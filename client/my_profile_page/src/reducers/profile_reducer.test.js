import profile_reducer from './profile_reducer'
import initialState from './selectors'

it('should return the initial state if nothing passed into reducer', () => {

  expect(profile_reducer(initialState, {})).toEqual({
    mainUserId:0,
    userId: 0,
    username: '',
    image: '',
    nextUrl: '/api/user'  + '/post/',
    nextUrlNum: 0,
    loadingMore: false,
    fullList: false,
    lid: 0,
    posts: [],
    tagList: [],
    following: []
  })
})

it('set user info', () => {
  let action = {
    type: 'SET_USER_INFO',
    id: 1,
    username: 'user1',
    image: 'image1'
  }
  let newState = profile_reducer(initialState, action)
  expect(newState).toEqual({
    ...initialState,
    userId: 1,
    username: 'user1',
    image: 'image1',
  })
})

it('set profile user id', () => {
  let action = {
    type: 'SET_profile_USER_ID',
    id: 0
  }
  expect(profile_reducer(initialState, action)).toEqual({
    ...initialState,
    mainUserId: 0
  })
})

it('set is full', () => {
  let action = {
    type: 'SET_IS_FULL',
    bool: []
  }
  expect(profile_reducer(initialState, action)).toEqual({
    ...initialState,
    fullList: false
  })
})

it('set last post id', () => {
  let pushState = {
    ...initialState,
    posts: [{
      body: "body1",
      created: "created1",
      id: 0,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies: [{reply: "reply"}],
      title: "title1",
      username: "user1"}]
  }
  let action = {
    type: 'SET_LAST_POST_ID',
    posts: [
      {
        body: "body1",
        created: "created1",
        id: 0,
        likes: 0,
        nextReplies: undefined,
        nextRepliesNum: 0,
        replies: [{reply: "reply"}],
        title: "title1",
        username: "user1"}
    ]
  }
  let newState = profile_reducer(pushState, action)

  expect(newState).toEqual({
    ...initialState,
    posts: [{
      body: "body1",
      created: "created1",
      id: 0,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies: [{reply: "reply"}],
      title: "title1",
      username: "user1"}],
    lid: 0
  })
})

it('set tag list', () => {
  expect(profile_reducer(initialState, {
    type: 'SET_TAG_LIST',
    tagList: []})).toEqual({
    ...initialState,
    tagList: undefined
  })
})

it('push following', () => {
  let action = {
    type: 'PUSH_FOLLOWING',
    followingId: 0
  }
  let newState = profile_reducer(initialState, action)
  expect(newState).toEqual({
    ...initialState,
    following: [{
      id: 0,
      image: undefined,
      username: undefined
    }]
  })
})

it('push posts', () => {
  let action = {
    type: 'PUSH_POSTS',
    id: 0,
    username: 'user1',
    title: 'title1',
    body: 'body1',
    created: 'created1',
    replies: [],
    nextReplies: [],
    nextRepliesNum: 0,
    likes: 0
  }
  let newState = profile_reducer(initialState, action)
  expect(newState).toEqual({
      following: [],
      fullList: true, image: '',
      lid: 0, loadingMore: false,
      mainUserId: 0,
      nextUrl: "/api/user/post/",
      nextUrlNum: 1,


      posts: [{
        body: 'body1',
        created: 'created1',
        id: 0,
        likes: 0,
        nextReplies: [],
        nextRepliesNum: 0,
        replies: [],
        user: undefined,
        title: 'title1',
        tag: undefined,
        username: 'user1'}],
        tagList: [],
        userId: 0,
        username: ''})
})

it('push replies', () => {
  let pushState = {
    following: [],
    fullList: false,
    image: "",
    lid: 0,
    loadingMore: false,
    nextUrl: "/api/post",
    posts: [{
      body: 'body1',
      created: 'created1',
      id: 0,
      likes: [{like: undefined}],
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies: [],
      title: 'title1',
      username: 'user1'},
      {
        body: 'body2',
        created: 'created2',
        id: 1,
        likes: [{like: undefined}],
        nextReplies: undefined,
        nextRepliesNum: 0,
        replies: [],
        title: 'title2',
        username: 'user2'},
      {
        body: 'body3',
        created: 'created3',
        id: 2,
        likes: [{like: undefined}],
        nextReplies: undefined,
        nextRepliesNum: 0,
        replies: [{reply: 'reply'}],
        title: 'title3',
        username: 'user3'}],
    tagList: [],
    userId: 0,
    username: ""
  }

  let action = {
    type: 'PUSH_REPLIES',
    id: 0,
    pid: 1,
    like: [{like: undefined}],
    replies: [{reply:'reply1'}, {reply: 'reply2'}]
  }

  let newState = profile_reducer(pushState, action)

  expect(newState).toEqual({

      following: [],
      fullList: false,
      image: "",
      lid: 0,
      loadingMore: false,
      nextUrl: "/api/post",
      posts: [{
        body: 'body1',
        created: 'created1',
        id: 0,
        likes: [{like: undefined}],
        nextReplies: undefined,
        nextRepliesNum: 0,
        replies: [],
        title: 'title1',
        username: 'user1'},
        {
          body: 'body2',
          created: 'created2',
          id: 1,
          likes: [{like: undefined}],
          nextReplies: undefined,
          nextRepliesNum: 1,
          replies:[
             {
              body: undefined,
              created: undefined,
              id: undefined,
              like: [
                {
                  like: undefined,
                },
              ],
              popular: false,
              post: undefined,
              user: undefined,
              username: undefined,
            },
          ],

          title: 'title2',
          username: 'user2'},
          {
            body: 'body3',
            created: 'created3',
            id: 2,
            likes: [{like: undefined}],
            nextReplies: undefined,
            nextRepliesNum: 0,
            replies: [{reply: 'reply'}],
            title: 'title3',
            username: 'user3'}],
      tagList: [],
      userId: 0,
      username: ""})
})

it('push replies2', () => {
  let pushState = {
    following: [],
    fullList: false,
    image: "",
    lid: 0,
    loadingMore: false,
    nextUrl: "/api/post",
    posts: [{
      body: 'body1',
      created: 'created1',
      id: 0,
      likes: [{like: undefined}],
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies: [],
      title: 'title1',
      username: 'user1'},
      {
        body: 'body2',
        created: 'created2',
        id: 1,
        likes: [{like: undefined}],
        nextReplies: undefined,
        nextRepliesNum: 0,
        replies: [],
        title: 'title2',
        username: 'user2'},
      {
        body: 'body3',
        created: 'created3',
        id: 2,
        likes: [{like: undefined}],
        nextReplies: undefined,
        nextRepliesNum: 0,
        replies: [{reply: 'reply2'}, {reply: 'reply3'}, {reply: 'reply4'}, {reply: 'reply5'}, {reply: 'reply6'}, {reply: 'reply7'}, {reply: 'reply8'}, {reply: 'reply9'}, {reply: 'reply10'}],
        title: 'title3',
        username: 'user3'}],
    tagList: [],
    userId: 0,
    username: ""
  }

  let action = {
    type: 'PUSH_REPLIES',
    id: 0,
    rid: 0,
    pid: 2,
    user: 0,
    username: '',
    created: '',
    post: undefined,
    body: '',
    like: [{like: undefined}, {like: undefined}],
    replies: [{reply:'reply1'}, {reply: 'reply2'}]
  }

  let newState = profile_reducer(pushState, action)

  expect(newState).toEqual({

      following: [],
      fullList: false,
      image: "",
      lid: 0,
      loadingMore: false,
      nextUrl: "/api/post",
      posts: [{
        body: 'body1',
        created: 'created1',
        id: 0,
        likes: [{like: undefined}],
        nextReplies: undefined,
        nextRepliesNum: 0,
        replies: [],
        title: 'title1',
        username: 'user1'},
        {
          body: 'body2',
          created: 'created2',
          id: 1,
          likes: [{like: undefined}],
          nextReplies: undefined,
          nextRepliesNum: 0,
          replies: [],
          title: 'title2',
          username: 'user2'},
          {
            body: 'body3',
            created: 'created3',
            id: 2,
            likes: [{like: undefined}],
            nextReplies: undefined,
            nextRepliesNum: 1,
            replies: [{reply: 'reply2'}, {reply: 'reply3'}, {reply: 'reply4'}, {reply: 'reply5'}, {reply: 'reply6'}, {reply: 'reply7'}, {reply: 'reply8'}, {reply: 'reply9'}, {reply: 'reply10'},
            {
                body: "",
                created: "",
                id: 0,
                like: [
                   {
                    like: undefined,
                  },
                  {
                    like: undefined
                  }
                ],
                popular: true,
                post: undefined,
                user: 0,
                username: "",
              }],
            title: 'title3',
            username: 'user3'}],
      tagList: [],
      userId: 0,
      username: ""})
})


it('update reply list', () => {
  let pushState = {
    following: [],
    fullList: false,
    image: "",
    lid: 0,
    loadingMore: false,
    nextUrl: "/api/post",
    posts: [{
      body: 'body1',
      created: 'created1',
      id: 0,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies: [],
      title: 'title1',
      username: 'user1'},
    {
      body: 'body2',
      created: 'created2',
      id: 1,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies: [],
      title: 'title2',
      username: 'user2'},
    {
      body: 'body3',
      created: 'created3',
      id: 2,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies: [{reply: 'reply'}],
      title: 'title3',
      username: 'user3'}],
    tagList: [],
    userId: 0,
    username: ""
  }
  let action = {
    type: 'UPDATE_REPLY_LIST',
    id: 0,
    replies: {reply: 'reply'}
  }
  let newState = profile_reducer(pushState, action)
  expect(newState).toEqual({

      following: [],
      fullList: false,
      image: "",
      lid: 0,
      loadingMore: false,
      nextUrl: "/api/post",
      posts: [{
        body: "body1",
        created: "created1",
        id: 0,
        likes: 0,
        nextReplies: undefined,
        nextRepliesNum: 0,
        replies: [{
          body: undefined,
          created: undefined,
          id: undefined,
          like: undefined,
          popular: false,
          post: undefined,
          user: undefined,
          username: undefined}],
        title: "title1",
        username: "user1"},
      {
        body: "body2",
        created: "created2",
        id: 1,
        likes: 0,
        nextReplies: undefined,
        nextRepliesNum: 0,
        replies: [],
        title: "title2",
        username: "user2"},
      {
        body: "body3",
        created: "created3",
        id: 2,
        likes: 0,
        nextReplies: undefined,
        nextRepliesNum: 0,
        replies: [{reply: 'reply'}],
        title: "title3",
        username: "user3"}],
        tagList: [],
        userId: 0,
        username: ""})
})

it('update reply list2', () => {
  let pushState = {
    following: [],
    fullList: false,
    image: "",
    lid: 0,
    loadingMore: false,
    nextUrl: "/api/post",
    posts: [{
      body: 'body1',
      created: 'created1',
      id: 0,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies: [],
      title: 'title1',
      username: 'user1'},
    {
      body: 'body2',
      created: 'created2',
      id: 1,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies: [],
      title: 'title2',
      username: 'user2'},
    {
      body: 'body3',
      created: 'created3',
      id: 2,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies: [{reply: 'reply'}],
      title: 'title3',
      username: 'user3'}],
    tagList: [],
    userId: 0,
    username: ""
  }
  let action = {
    type: 'UPDATE_REPLY_LIST',
    id: 2,
    replies: {reply: 'reply2'}
  }
  let newState = profile_reducer(pushState, action)
  expect(newState).toEqual({

      following: [],
      fullList: false,
      image: "",
      lid: 0,
      loadingMore: false,
      nextUrl: "/api/post",
      posts: [{
        body: "body1",
        created: "created1",
        id: 0,
        likes: 0,
        nextReplies: undefined,
        nextRepliesNum: 0,
        replies: [],
        title: "title1",
        username: "user1"},
      {
        body: "body2",
        created: "created2",
        id: 1,
        likes: 0,
        nextReplies: undefined,
        nextRepliesNum: 0,
        replies: [],
        title: "title2",
        username: "user2"},
      {
        body: "body3",
        created: "created3",
        id: 2,
        likes: 0,
        nextReplies: undefined,
        nextRepliesNum: 0,
        replies: [
          {reply: "reply"},
          {
            body: undefined,
            created: undefined,
            id: undefined,
            like: undefined,
            popular: false,
            post: undefined,
            user: undefined,
            username: undefined,
}],
        title: "title3",
        username: "user3"}],
        tagList: [],
        userId: 0,
        username: ""})
})


it('update reply list3', () => {
  let pushState = {
    following: [],
    fullList: false,
    image: "",
    lid: 0,
    loadingMore: false,
    nextUrl: "/api/post",
    posts: [{
      body: 'body1',
      created: 'created1',
      id: 0,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies: [{reply: 'reply1'}, {reply: 'reply2'}, {reply: 'reply3'}, {reply: 'reply4'}, {reply: 'reply5'}, {reply: 'reply6'}, {reply: 'reply7'}, {reply: 'reply8'}, {reply: 'reply9'}],
      title: 'title1',
      username: 'user1'},
    {
      body: 'body2',
      created: 'created2',
      id: 1,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies: [],
      title: 'title2',
      username: 'user2'},
    {
      body: 'body3',
      created: 'created3',
      id: 2,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies: [{reply: 'reply'}],
      title: 'title3',
      username: 'user3'}],
    tagList: [],
    userId: 0,
    username: ""
  }
  let action = {
    type: 'UPDATE_REPLY_LIST',
    id: 0,
    replies: {reply: 'reply10'}
  }
  let newState = profile_reducer(pushState, action)
  expect(newState).toEqual({

      following: [],
      fullList: false,
      image: "",
      lid: 0,
      loadingMore: false,
      nextUrl: "/api/post",
      posts: [{
        body: "body1",
        created: "created1",
        id: 0,
        likes: 0,
        nextReplies: undefined,
        nextRepliesNum: 0,
        replies: [{reply: 'reply1'}, {reply: 'reply2'}, {reply: 'reply3'}, {reply: 'reply4'}, {reply: 'reply5'}, {reply: 'reply6'}, {reply: 'reply7'}, {reply: 'reply8'}, {reply: 'reply9'}, {
            body: undefined,
            created: undefined,
            id: undefined,
            like: undefined,
            popular: false,
            post: undefined,
            user: undefined,
            username: undefined,
}],
        title: "title1",
        username: "user1"},
      {
        body: "body2",
        created: "created2",
        id: 1,
        likes: 0,
        nextReplies: undefined,
        nextRepliesNum: 0,
        replies: [],
        title: "title2",
        username: "user2"},
      {
        body: "body3",
        created: "created3",
        id: 2,
        likes: 0,
        nextReplies: undefined,
        nextRepliesNum: 0,
        replies: [{reply: "reply"}],
        title: "title3",
        username: "user3"}],
        tagList: [],
        userId: 0,
        username: ""})
})


it('update reply list3', () => {
  let pushState = {
    following: [],
    fullList: false,
    image: "",
    lid: 0,
    loadingMore: false,
    nextUrl: "/api/post",
    posts: [{
      body: 'body1',
      created: 'created1',
      id: 0,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies: [{reply: 'reply1'}, {reply: 'reply2'}, {reply: 'reply3'}, {reply: 'reply4'}, {reply: 'reply5'}, {reply: 'reply6'}, {reply: 'reply7'}, {reply: 'reply8'}, {reply: 'reply9'}, {reply: 'reply10'}],
      title: 'title1',
      username: 'user1'},
    {
      body: 'body2',
      created: 'created2',
      id: 1,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies: [],
      title: 'title2',
      username: 'user2'},
    {
      body: 'body3',
      created: 'created3',
      id: 2,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies: [{reply: 'reply'}],
      title: 'title3',
      username: 'user3'}],
    tagList: [],
    userId: 0,
    username: ""
  }
  let action = {
    type: 'UPDATE_REPLY_LIST',
    id: 0,
    nextReplies: 1,
    replies: {reply: 'reply11'}
  }
  let newState = profile_reducer(pushState, action)
  expect(newState).toEqual({

      following: [],
      fullList: false,
      image: "",
      lid: 0,
      loadingMore: false,
      nextUrl: "/api/post",
      posts: [{
        body: "body1",
        created: "created1",
        id: 0,
        likes: 0,
        nextReplies: undefined,
        nextRepliesNum: 0,
        replies: [{reply: 'reply1'}, {reply: 'reply2'}, {reply: 'reply3'}, {reply: 'reply4'}, {reply: 'reply5'}, {reply: 'reply6'}, {reply: 'reply7'}, {reply: 'reply8'}, {reply: 'reply9'}, {reply: 'reply10'}],
        title: "title1",
        username: "user1"},
      {
        body: "body2",
        created: "created2",
        id: 1,
        likes: 0,
        nextReplies: undefined,
        nextRepliesNum: 0,
        replies: [],
        title: "title2",
        username: "user2"},
      {
        body: "body3",
        created: "created3",
        id: 2,
        likes: 0,
        nextReplies: undefined,
        nextRepliesNum: 0,
        replies: [{reply: "reply"}],
        title: "title3",
        username: "user3"}],
        tagList: [],
        userId: 0,
        username: ""})
})

it('update reply list4', () => {
  let pushState = {
    following: [],
    fullList: false,
    image: "",
    lid: 0,
    loadingMore: false,
    nextUrl: "/api/post",
    posts: [{
      body: 'body1',
      created: 'created1',
      id: 0,
      likes: 0,
      nextRepliesNum: 0,
      replies: [{reply: 'reply1'}, {reply: 'reply2'}, {reply: 'reply3'}, {reply: 'reply4'}, {reply: 'reply5'}, {reply: 'reply6'}, {reply: 'reply7'}, {reply: 'reply8'}, {reply: 'reply9'}, {reply: 'reply10'}],
      title: 'title1',
      username: 'user1'},
    {
      body: 'body2',
      created: 'created2',
      id: 1,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies: [],
      title: 'title2',
      username: 'user2'},
    {
      body: 'body3',
      created: 'created3',
      id: 2,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies: [{reply: 'reply'}],
      title: 'title3',
      username: 'user3'}],
    tagList: [],
    userId: 0,
    username: ""
  }
  let action = {
    type: 'UPDATE_REPLY_LIST',
    id: 0,
    nextReplies: 1,
    replies: {reply: 'reply11'}
  }
  let newState = profile_reducer(pushState, action)
  expect(newState).toEqual({

      following: [],
      fullList: false,
      image: "",
      lid: 0,
      loadingMore: false,
      nextUrl: "/api/post",
      posts: [{
        body: "body1",
        created: "created1",
        id: 0,
        likes: 0,
        nextReplies: undefined,
        nextRepliesNum: 0,
        replies: [{reply: 'reply1'}, {reply: 'reply2'}, {reply: 'reply3'}, {reply: 'reply4'}, {reply: 'reply5'}, {reply: 'reply6'}, {reply: 'reply7'}, {reply: 'reply8'}, {reply: 'reply9'}, {reply: 'reply10'}],
        title: "title1",
        username: "user1"},
      {
        body: "body2",
        created: "created2",
        id: 1,
        likes: 0,
        nextReplies: undefined,
        nextRepliesNum: 0,
        replies: [],
        title: "title2",
        username: "user2"},
      {
        body: "body3",
        created: "created3",
        id: 2,
        likes: 0,
        nextReplies: undefined,
        nextRepliesNum: 0,
        replies: [{reply: "reply"}],
        title: "title3",
        username: "user3"}],
        tagList: [],
        userId: 0,
        username: ""})
})

it('delete reply from list', () => {
  let pushState = {
    following: [],
    fullList: false,
    image: "",
    lid: 0,
    loadingMore: false,
    nextUrl: "/api/post",
    posts: [{
      body: 'body1',
      created: 'created1',
      id: 0,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies: [],
      title: 'title1',
      username: 'user1'},
    {
      body: 'body2',
      created: 'created2',
      id: 1,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies: [],
      title: 'title2',
      username: 'user2'},
    {
      body: 'body3',
      created: 'created3',
      id: 2,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies: [{reply: 'reply'}],
      title: 'title3',
      username: 'user3'}],
    tagList: [],
    userId: 0,
    username: ""
  }
  let action = {
    type: 'DELETE_REPLY_FROM_LIST',
    pid: 2,
    id: 0
  }
  let newState = profile_reducer(pushState, action)
  expect(newState).toEqual({
    following: [],
    fullList: false,
    image: "",
    lid: 0,
    loadingMore: false,
    nextUrl: "/api/post",
    posts: [{
      body: 'body1',
      created: 'created1',
      id: 0,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies: [],
      title: 'title1',
      username: 'user1'},
    {
      body: 'body2',
      created: 'created2',
      id: 1,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies: [],
      title: 'title2',
      username: 'user2'},
    {
      body: 'body3',
      created: 'created3',
      id: 2,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 1,
      replies: [
        {
          reply: 'reply'
        },
        undefined
      ],
      title: 'title3',
      username: 'user3'}],
    tagList: [],
    userId: 0,
    username: ""
  })
})


it('delete reply from list2', () => {
  let pushState = {
    following: [],
    fullList: false,
    image: "",
    lid: 0,
    loadingMore: false,
    nextUrl: "/api/post",
    posts: [{
      body: 'body1',
      created: 'created1',
      id: 0,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies: [],
      title: 'title1',
      username: 'user1'},
    {
      body: 'body2',
      created: 'created2',
      id: 1,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies: [],
      title: 'title2',
      username: 'user2'},
    {
      body: 'body3',
      created: 'created3',
      id: 2,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies: [{reply: 'reply'}],
      title: 'title3',
      username: 'user3'}],
    tagList: [],
    userId: 0,
    username: ""
  }
  let action = {
    type: 'DELETE_REPLY_FROM_LIST',
    pid: 1,
    id: 0
  }
  let newState = profile_reducer(pushState, action)
  expect(newState).toEqual({
    following: [],
    fullList: false,
    image: "",
    lid: 0,
    loadingMore: false,
    nextUrl: "/api/post",
    posts: [{
      body: 'body1',
      created: 'created1',
      id: 0,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies: [],
      title: 'title1',
      username: 'user1'},
    {
      body: 'body2',
      created: 'created2',
      id: 1,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 1,
      replies: [undefined,],
      title: 'title2',
      username: 'user2'},
    {
      body: 'body3',
      created: 'created3',
      id: 2,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies: [
        {
          reply: 'reply'
        }],
      title: 'title3',
      username: 'user3'}],
    tagList: [],
    userId: 0,
    username: ""
  })
})

it('delete reply from list3', () => {
  let pushState = {
    following: [],
    fullList: false,
    image: "",
    lid: 0,
    loadingMore: false,
    nextUrl: "/api/post",
    posts: [{
      body: 'body1',
      created: 'created1',
      id: 0,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies: [],
      title: 'title1',
      username: 'user1'},
    {
      body: 'body2',
      created: 'created2',
      id: 1,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies: [],
      title: 'title2',
      username: 'user2'},
    {
      body: 'body3',
      created: 'created3',
      id: 2,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies: [{id: 0, reply: 'reply'}, {id: 1, reply: 'reply1'}],
      title: 'title3',
      username: 'user3'}],
    tagList: [],
    userId: 0,
    username: ""
  }
  let action = {
    type: 'DELETE_REPLY_FROM_LIST',
    nextReplies: null,
    newReply: {id: 2, reply: 'reply1'},
    pid: 2,
    id: 0
  }
  let newState = profile_reducer(pushState, action)
  expect(newState).toEqual({
    following: [],
    fullList: false,
    image: "",
    lid: 0,
    loadingMore: false,
    nextUrl: "/api/post",
    posts: [{
      body: 'body1',
      created: 'created1',
      id: 0,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies: [],
      title: 'title1',
      username: 'user1'},
    {
      body: 'body2',
      created: 'created2',
      id: 1,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies: [],
      title: 'title2',
      username: 'user2'},
    {
      body: 'body3',
      created: 'created3',
      id: 2,
      likes: 0,
      nextReplies: null,
      nextRepliesNum: 1,
      replies: [
        {
          id: 1,
          reply: 'reply1'
        },
        {
          id: 2,
          reply: 'reply1'
        }
      ],
      title: 'title3',
      username: 'user3'}],
    tagList: [],
    userId: 0,
    username: ""
  })
})

it('delete post from list', () => {
  let pushState = {
    following: [],
    fullList: false,
    image: "",
    lid: 0,
    loadingMore: false,
    nextUrl: "/api/post",
    posts: [{
      body: 'body1',
      created: 'created1',
      id: 0,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies: [],
      title: 'title1',
      username: 'user1'},
    {
      body: 'body2',
      created: 'created2',
      id: 1,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies: [],
      title: 'title2',
      username: 'user2'},
    {
      body: 'body3',
      created: 'created3',
      id: 2,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies: [{reply: 'reply'}],
      title: 'title3',
      username: 'user3'}],
    tagList: [],
    userId: 0,
    username: ""
  }
  let action = {
    type: 'DELETE_POST_FROM_LIST',
    newPost: {id: 0, username: '', user: 0, title: '', body: '', created: '', tag: ''},
    id: 0
  }

  let newState = profile_reducer(pushState, action)
  expect(newState).toEqual({
    following: [],
    fullList: false,
    image: "",
    lid: 0,
    loadingMore: false,
    nextUrl: undefined,
    nextUrlNum: 1,
    posts: [{
      body: 'body2',
      created: 'created2',
      id: 1,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies:[],
      title: "title2",
      username: "user2"},
      {
        body: "body3",
        created: "created3",
        id: 2,
        likes: 0,
        nextReplies: undefined,
        nextRepliesNum: 0,
        replies: [{reply: "reply"}],
        title: "title3",
        username: "user3"},
    {
      body: "",
      created: "",
      id: 0,
      likes: [],
      nextReplies: null,
      nextRepliesNum: 0,
      replies: [],
      tag: "",
      title: "",
      user: 0,
      username: ""}],
      tagList: [],
      userId: 0,
      username: ""}

  )
})

it('delete post from list2', () => {
  let pushState = {
    following: [],
    fullList: false,
    image: "",
    lid: 0,
    loadingMore: false,
    nextUrl: "/api/post",
    posts: [{
      body: 'body1',
      created: 'created1',
      id: 0,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies: [],
      title: 'title1',
      username: 'user1'},
    {
      body: 'body2',
      created: 'created2',
      id: 1,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies: [],
      title: 'title2',
      username: 'user2'},
    {
      body: 'body3',
      created: 'created3',
      id: 2,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies: [{reply: 'reply'}],
      title: 'title3',
      username: 'user3'}],
    tagList: [],
    userId: 0,
    username: ""
  }
  let action = {
    type: 'DELETE_POST_FROM_LIST',
    nextUrl: null,
    newPost: {id: 0, username: '', user: 0, title: '', body: '', created: '', tag: ''},
    id: 0
  }

  let newState = profile_reducer(pushState, action)
  expect(newState).toEqual({
    following: [],
    fullList: false,
    image: "",
    lid: 0,
    loadingMore: false,
    nextUrl: null,
    nextUrlNum: 1,
    posts: [{
      body: 'body2',
      created: 'created2',
      id: 1,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies:[],
      title: "title2",
      username: "user2"},
      {
        body: "body3",
        created: "created3",
        id: 2,
        likes: 0,
        nextReplies: undefined,
        nextRepliesNum: 0,
        replies: [{reply: "reply"}],
        title: "title3",
        username: "user3"},
    {
      body: "",
      created: "",
      id: 0,
      likes: [],
      nextReplies: null,
      nextRepliesNum: 0,
      replies: [],
      tag: "",
      title: "",
      user: 0,
      username: ""}],
      tagList: [],
      userId: 0,
      username: ""}

  )
})

it('update reply like list', () => {
  let pushState = {
    following: [],
    fullList: false,
    image: "",
    lid: 0,
    loadingMore: false,
    nextUrl: "/api/post",
    posts: [{
      body: 'body1',
      created: 'created1',
      id: 0,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies: [],
      title: 'title1',
      username: 'user1'},
    {
      body: 'body2',
      created: 'created2',
      id: 1,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies: [],
      title: 'title2',
      username: 'user2'},
    {
      body: 'body3',
      created: 'created3',
      id: 2,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies: [{reply: 'reply'}],
      title: 'title3',
      username: 'user3'}],
    tagList: [],
    userId: 0,
    username: ""
  }

  let action = {
    type: 'UPDATE_REPLY_LIKE_LIST',
    id: 1,
    likes: 1,
    pid: 2,
    like: [{like: 0}, {like: 1}]
  }
  let newState = profile_reducer(pushState, action)
  expect(newState).toEqual({

    following: [],
    fullList: false,
    image: "",
    lid: 0,
    loadingMore: false,
    nextUrl: "/api/post",
    posts: [{
      body: 'body1',
      created: 'created1',
      id: 0,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies: [],
      title: 'title1',
      username: 'user1'},
    {
      body: 'body2',
      created: 'created2',
      id: 1,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies: [],
      title: 'title2',
      username: 'user2'},
    {
      body: 'body3',
      created: 'created3',
      id: 2,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies: [{like: [{like: 0}, {like: 1}],popular: true, reply: 'reply'}],
      title: 'title3',
      username: 'user3'}],
    tagList: [],
    userId: 0,
    username: ""
  })
})

it('update reply like list2', () => {
  let pushState = {
    following: [],
    fullList: false,
    image: "",
    lid: 0,
    loadingMore: false,
    nextUrl: "/api/post",
    posts: [{
      body: 'body1',
      created: 'created1',
      id: 0,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies: [],
      title: 'title1',
      username: 'user1'},
    {
      body: 'body2',
      created: 'created2',
      id: 1,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies: [],
      title: 'title2',
      username: 'user2'},
    {
      body: 'body3',
      created: 'created3',
      id: 2,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies: [{reply: 'reply'}],
      title: 'title3',
      username: 'user3'}],
    tagList: [],
    userId: 0,
    username: ""
  }

  let action = {
    type: 'UPDATE_REPLY_LIKE_LIST',
    id: 1,
    likes: 1,
    pid: 2,
    rid: 2,
    like: [{like: 0}, {like: 1}]
  }
  let newState = profile_reducer(pushState, action)
  expect(newState).toEqual({

    following: [],
    fullList: false,
    image: "",
    lid: 0,
    loadingMore: false,
    nextUrl: "/api/post",
    posts: [{
      body: 'body1',
      created: 'created1',
      id: 0,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies: [],
      title: 'title1',
      username: 'user1'},
    {
      body: 'body2',
      created: 'created2',
      id: 1,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies: [],
      title: 'title2',
      username: 'user2'},
    {
      body: 'body3',
      created: 'created3',
      id: 2,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies: [{reply: 'reply'}],
      title: 'title3',
      username: 'user3'}],
    tagList: [],
    userId: 0,
    username: ""
  })
})

it('update like list', () => {
  let pushState = {
    posts: [{
      body: 'body1',
      created: 'created1',
      id: 0,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies: [],
      title: 'title1',
      username: 'user1'},
    {
      body: 'body2',
      created: 'created2',
      id: 1,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies: [],
      title: 'title2',
      username: 'user2'},
    {
      body: 'body3',
      created: 'created3',
      id: 2,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies: [{reply: 'reply'}],
      title: 'title3',
      username: 'user3'}]
  }
  let action = {
    type: 'UPDATE_LIKE_LIST',
    id: 1,
    likes: [{like: 1}]
  }
  let newState = profile_reducer(pushState, action)
  expect(newState).toEqual({
    posts: [{
      body: 'body1',
      created: 'created1',
      id: 0,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies: [],
      title: 'title1',
      username: 'user1'},
    {
      body: 'body2',
      created: 'created2',
      id: 1,
      likes: [{like: 1}],
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies: [],
      title: 'title2',
      username: 'user2'},
    {
      body: 'body3',
      created: 'created3',
      id: 2,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies: [{reply: 'reply'}],
      title: 'title3',
      username: 'user3'}]
  })
})

it('update next url', () => {
  let pushState = {
    following: [],
    fullList: false,
    image: "",
    lid: 0,
    loadingMore: false,
    nextUrl: "/api/post",
    posts: [{
      body: 'body1',
      created: 'created1',
      id: 0,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies: [],
      title: 'title1',
      username: 'user1'},
    {
      body: 'body2',
      created: 'created2',
      id: 1,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies: [],
      title: 'title2',
      username: 'user2'},
    {
      body: 'body3',
      created: 'created3',
      id: 2,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies: [{reply: 'reply'}],
      title: 'title3',
      username: 'user3'}],
    tagList: [],
    userId: 0,
    username: ""
  }

  let action = {
    type: 'UPDATE_NEXT_URL',
    url: 'url1'
  }

  let newState = profile_reducer(pushState, action)
  expect(newState).toEqual({

    following: [],
    fullList: false,
    image: "",
    lid: 0,
    loadingMore: false,
    nextUrl: "url1",
    posts: [{
      body: 'body1',
      created: 'created1',
      id: 0,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies: [],
      title: 'title1',
      username: 'user1'},
    {
      body: 'body2',
      created: 'created2',
      id: 1,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies: [],
      title: 'title2',
      username: 'user2'},
    {
      body: 'body3',
      created: 'created3',
      id: 2,
      likes: 0,
      nextReplies: undefined,
      nextRepliesNum: 0,
      replies: [{reply: 'reply'}],
      title: 'title3',
      username: 'user3'}],
    tagList: [],
    userId: 0,
    username: ""
  })
})

it('toggle load', () => {
  let action = {
    type: 'TOGGLE_LOAD',
    toggle: true
  }

  let newState = profile_reducer(initialState, action)

  expect(newState).toEqual({

      following: [],
      fullList: false,
      image: "",
      lid: 0,
      loadingMore: true,

      mainUserId: 0,
      nextUrl: "/api/user/post/",
      nextUrlNum: 0,

      posts: [],
      tagList: [],
      userId: 0,
      username: ""})
})
