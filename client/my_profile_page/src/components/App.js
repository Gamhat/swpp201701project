import React from 'react'
import Navbar from '../containers/navbar'
import UserInfo from '../containers/user'
import PostsList from '../containers/post_list'
import ChangeProfile from '../containers/changeprofile'
import Followers from '../containers/followers'
import { Screen, Main, Container, SecContainer, Posts } from './css'

const App = () => {
    return (
        <Screen>
            <Navbar />
            <Main>
                <UserInfo />
                <Container>
                    <Posts>
                        <PostsList />
                    </Posts>
                    <SecContainer>
                        <ChangeProfile/>
                        <Followers />
                    </SecContainer>
                </Container>
            </Main>
        </Screen>
    );
}

export default App
