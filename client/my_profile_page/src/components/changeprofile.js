import React, { Component } from 'react'
import { Profile, EmptyProfile, Button, Input, Preview, Img } from './css'

class ChangeProfile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            file: '',
            imagePreview: ''
        };
        this._handleImageChange = this._handleImageChange.bind(this);
        this._handleSubmit = this._handleSubmit.bind(this);
    }

    _handleSubmit(e) {
        e.preventDefault()
        if (this.state.file !== '') {
            this.props.addImage(this.state.file)
        }
    }

    _handleImageChange(e) {
        e.preventDefault()

        let reader = new FileReader()
        let file = e.target.files[0];

        reader.onloadend = () => {
            this.setState({
                file: file,
                imagePreview: reader.result
            });
        }

        reader.readAsDataURL(file)
    }

    render() {
        let {imagePreview} = this.state;
        let imagePrev = null;

        if (imagePreview) {
            imagePrev = (<Img className="image_preview" src={imagePreview} />);
        }
        else {
            imagePrev = (<div style={{"margin-top": "30%"}} className="previewText">Please select an Image</div>);
        }
       
        return (
            <Profile className="change_profile">
                <form onSubmit={(e) => this._handleSubmit(e)}>
                    <Input id="fileInput" type="file" 
                        onChange={(e) => this._handleImageChange(e)} />
		        <Preview className="image_preview">
                    {imagePrev}
                </Preview>
                    <Button id="upload_image_button" type="submit" 
                        onClick={(e) => this._handleSubmit(e)}>
                        Upload Profile Image</Button>
                </form>
                
        </Profile>
        )
    }
}

export default ChangeProfile
