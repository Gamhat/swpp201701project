import { connect } from 'react-redux'
import UserInfo from '../components/user'
import { gotoProfile } from '../actions/actions'

const mapStateToProps = (state) => {
    return {
        userId: state.userId,
        username: state.username,
        following: state.following,
        image: state.image
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        gotoProfile: () => {
            dispatch(gotoProfile())
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserInfo)
