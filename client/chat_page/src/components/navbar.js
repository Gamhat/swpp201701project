import React from 'react'
import { UL, A, A2, LI2, Logo } from './css'

const Navbar = ({ logout }) => {
    return (
        <UL className="navigation bar">
		    <Logo><A2 href='/' id="main_button">Folivora</A2></Logo>       
		    <LI2>
                <A id="logout_button" onClick={logout}>Logout</A>
            </LI2>
        </UL>
    )
}

export default Navbar
