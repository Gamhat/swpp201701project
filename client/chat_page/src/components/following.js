import React from 'react'
import Follow from './follow'
import { FollowBox, BoxTitle } from './css'

const Following = ({following, setFriendInfo}) => {
    const renderFollow = (following) => {
        if (following.length > 0) {
            return following.map((follow) => {
                return (
                    <Follow key={follow.id} {...follow}
                        setFriendInfo={(a,b,c) => setFriendInfo(a,b,c)}
                    />
                )
            })
        }
    }
    
    return (
        <FollowBox>
            <BoxTitle>My Following List</BoxTitle>
            {renderFollow(following)}
        </FollowBox>
    )
}

export default Following
