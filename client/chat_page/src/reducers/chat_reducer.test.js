import initialState from './selectors'
import chat_reducer from './chat_reducer'

it('initialState', () => {
  expect(chat_reducer(initialState, {})).toEqual({
    userId: 0,
    username: '',
    image: '',
    friendUserId: 0,
    friendUsername: '',
    friendImage: '',
    messages: [],
    following: []
  })
})

it('set user info', () => {
  let action = {
    type: 'SET_USER_INFO',
    id: 1,
    username: 'user1',
    image: 'image1'
  }
  let newState = chat_reducer(initialState, action)
  expect(newState).toEqual({
    ...initialState,
    userId: 1,
    username: 'user1',
    image: 'image1',
  })
})

it('push following', () => {
  let action = {
    type: 'PUSH_FOLLOWING',
    following: 0,
    user: 0,
    username: ''
  }
  let newState = chat_reducer(initialState, action)
  expect(newState).toEqual({
    ...initialState,
    following: [{
      id: 0,
      image: undefined,
      username: ''
    }]
  })
})

it('set friend info', () => {
  let action = {
    type: 'SET_FRIEND_INFO',
    id: 0,
    username: '',
    image: ''
  }
  expect(chat_reducer(initialState, action)).toEqual({
    ...initialState,
    friendUserId: 0,
    friendUsername: '',
    friendImage: ''
  })
})

it('push messages', () => {
  let action = {
    type: 'PUSH_MESSAGES',
    toData: [{data: 'a', id: 1}],
    fromData: {data: 'b', id: 0}
  }
  expect(chat_reducer(initialState, action)).toEqual({

      following: [],
      friendImage: "",
      friendUserId: 0,
      friendUsername: "",
      image: "",
      messages: [
        {
          data: "b",
          id: 0,
        },
        {
          data: 'a',
          id: 1
        }
      ],
      userId: 0,
      username: "",


  })
})

it('push messages2', () => {
  let action = {
    type: 'PUSH_MESSAGES',
    toData: [{data: 'a', id: 0}],
    fromData: {data: 'b', id: 1}
  }
  expect(chat_reducer(initialState, action)).toEqual({

      following: [],
      friendImage: "",
      friendUserId: 0,
      friendUsername: "",
      image: "",
      messages: [
        {
          data: "a",
          id: 0,
        },
        {
          data: "b",
          id: 1,
        },
      ],
      userId: 0,
      username: "",


  })
})
