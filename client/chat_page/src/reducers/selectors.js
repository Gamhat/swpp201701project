export const createNewInitialState = () => {
    return {
        userId: 0,
        username: '', 
        image: '',
        friendUserId: 0,
        friendUsername: '',
        friendImage: '',
        messages: [],
        following: []
    }
}

const initialState = createNewInitialState()

export default initialState
