import login_reducer from './login_reducer'
import initialState from './selectors'

it('should return the initial state if nothing passed into reducer', () => {
  expect(login_reducer(undefined, {})).toEqual({
      toggle: false,
      loginFail: false,
      joinFail: ''
  })
})

it('if toggle button is pressed', () => {
  expect(login_reducer(initialState, {type: 'JOIN_OR_LOGIN'})).toEqual({
      ...initialState,
      joinFail: '',
      loginFail: false,
      toggle: !initialState.toggle
  })
})

it('if login failed', () => {
  expect(login_reducer(initialState, {type: 'LOGIN_FAILURE'})).toEqual({
      ...initialState,
      loginFail: true
  })
})
