import initialState from './selectors'

const login_reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'JOIN_OR_LOGIN':
            return {
                ...state,
                joinFail: '',
                loginFail: false,
                toggle: !state.toggle
            }

        case 'LOGIN_FAILURE':
            return {
                ...state,
                loginFail: true
            }
        
        case 'JOIN_FAILURE':
            return {
                ...state,
                joinFail: action.joinFail
            }

        default:
            return state
    }
}

export default login_reducer
