export const createNewInitialState = () => {
    return {
        toggle: false,
        loginFail: false,
        joinFail: ''
    }
}

const initialState = createNewInitialState()

export default initialState
