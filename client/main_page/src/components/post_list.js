import React, { Component } from 'react'
import Post from './post'

class PostsList extends Component {
    componentWillMount() {
        this.props.updateState()
    }
   
    renderPosts(posts) {
        if (posts.length > 0) {
            return posts.map((post) => {
                return (
                    <Post key={post.id} {...post}
                        userId={this.props.userId}
                        tagList={this.props.tagList}
                        addReply={(a, b) => this.props.addReply(a, b)}
                        loadReply={(a, b) => this.props.loadReply(a, b)}
                        addLike={(a) => this.props.addLike(a)}
                        deleteLike={(a, b) => this.props.deleteLike(a, b)}
                        deleteReply={(a,b,c) => this.props.deleteReply(a,b,c)}
                        addReplyLike={(a,b) => this.props.addReplyLike(a,b)}
                        deleteReplyLike={(a,b,c) => this.props.deleteReplyLike(a,b,c)}
                    />    
                )
            })
        }
    }
    
    loadMore() {
        this.props.toggleLoad(true)
        this.props.fetchPosts(this.props.nextUrl)
        setTimeout(() => {
            this.props.toggleLoad(false)
        }, 2000)
    }

    renderWaypoint() {
        var Waypoint = require('react-waypoint')
        if(!this.props.fullList && !this.props.loadingMore) {
            return (
                <div className="waypoint">
                    <Waypoint
                        onEnter={this.loadMore.bind(this)}
                    />
                </div>
            )
        }
    }

    render() {
        return (
            <div className="container">
                <div className="render">
                    {this.renderPosts(this.props.posts)}
                    {this.renderWaypoint()}
                </div>
                <br></br>
                <br></br>
                <br></br>
           </div>
       )
    }
}

export default PostsList
