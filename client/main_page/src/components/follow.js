import React from 'react'
import { BoxA, BoxTab, BoxImage, FollowName } from './css'

const Follow = ({id, username, image, addFollow}) => {
    const getImage = () => {
        if (image === "") {
            return <BoxImage src="https://museum.wales/media/40374/thumb_480/empty-profile-grey.jpg" alt="User image" />
        }
        else {
            return <BoxImage src={image} alt="User image" />
        }
    }
    
    const clickFollow = () => {
        addFollow(id)
    }

    const followId = "click_follow_" + id

    return (
        <BoxA id={followId} type="submit" onClick={clickFollow}>
            <BoxTab>
                {getImage()}
                <FollowName>{username}</FollowName>
            </BoxTab>
        </BoxA>
    )
}

export default Follow
