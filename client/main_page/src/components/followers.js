import React from 'react'
import Follow from './follow'
import { FollowBox, BoxTitle } from './css'

const Followers = ({recFollows, addFollow}) => {
    const renderFollow = (recFollows) => {
        if (recFollows.length > 0) {
            return recFollows.map((follow) => {
                return (
                    <Follow key={follow.id} {...follow}
                        addFollow={(a) => addFollow(a)}
                    />
                )
            })
        }
    }
    
    return (
        <FollowBox>
            <BoxTitle>Recommended Friends</BoxTitle>
            {renderFollow(recFollows)}
        </FollowBox>
    )
}

export default Followers
