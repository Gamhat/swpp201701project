import { connect } from 'react-redux'
import Following from '../components/following'
import { newProfile } from '../actions/actions'

const mapStateToProps = (state) => {
    return {
        following: state.following
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        newProfile: (id) => {
            dispatch(newProfile(id))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Following)
