import { connect } from 'react-redux'
import PostsList from '../components/post_list'
import { updateState,  getInitialPost, addReply, loadReply, addLike, deletePost, deleteLike, deleteReply, addReplyLike, deleteReplyLike, toggleLoad } from '../actions/actions'

const mapStateToProps = (state) => {
    return {
        mainUserId: state.mainUserId,
        userId: state.userId,
        nextUrlNum: state.nextUrlNum,
        username: state.username,
        image: state.image,
        tagList: state.tagList,
        posts: state.posts,
        nextUrl: state.nextUrl,
        loadingMore: state.loadingMore,
        fullList: state.fullList
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        updateState: () => {
            dispatch(updateState())
        },

        fetchPosts: (url) => {
            dispatch(getInitialPost(url))
        },

        addReply: (id, text) => {
            dispatch(addReply(id, text))
        },

        loadReply: (id, url) => {
            dispatch(loadReply(id, url))
        },

        addLike: (id) => {
            dispatch(addLike(id))
        },

        deletePost: (id, num) => {
            dispatch(deletePost(id, num))
        },

        deleteLike: (id, likeId) => {
            dispatch(deleteLike(id, likeId))
        },

        deleteReply: (pid, id, num) => {
            dispatch(deleteReply(pid, id, num))
        },

        addReplyLike: (pid, rid) => {
            dispatch(addReplyLike(pid, rid))
        },

        deleteReplyLike: (pid, rid, lid) => {
            dispatch(deleteReplyLike(pid, rid, lid))
        },

        toggleLoad: (toggle) => {
            dispatch(toggleLoad(toggle))
        }
    }   
}

export default connect(mapStateToProps, mapDispatchToProps)(PostsList)
