from django.apps import AppConfig


class ServerApiConfig(AppConfig):
    name = 'server_api'

    def ready(self):
        from .bots import refresh
        refresh()
